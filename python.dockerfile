FROM python:3.8

WORKDIR /workdir

RUN pip install jsonpickle

COPY data ./data
COPY wiki.py ./

RUN python wiki.py --json 'covid'
