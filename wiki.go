package main

import (
    "fmt"
    "flag"
    "io/ioutil"
    "log"
    "math"
    "strings"
    "encoding/json"
    "time"
)

func main() {
    defer timeTrack(time.Now(), "main")
    lang := flag.String("lang", "en", "Language: en, it, fr")
    json_flag := flag.Bool("json", false, "Json output or not")
    limit := flag.Int("limit", 10, "Limit number of results")
    flag.Parse()
    input_string := strings.ToLower(flag.Args()[0])

    if !stringInSlice(lang, []string{"en", "it", "fr"}) {
        log.Fatal("Unsupported language: " + *lang)
    }

    path := "data/" + *lang

    data := read_language_file(&path)

    // Test: "covid" in English, should return 378 results.
    // This works for 1 thread, but the number of results decreases
    // when increasing the number of threads, even though chunks are
    // all correct.
    // It seems that the chunks before passing them to go search are correct
    // but they are not correct inside the search function. This is a bug
    number_of_threads := 4
    chunks := chunks(&data, number_of_threads)
    var channels [](chan []string)
    for _, chunk := range chunks {
        channel := make(chan []string, 1)
        channels = append(channels, channel)

        go search(chunk, &input_string, channel)
    }
    var filtered_data []string
    for _, channel := range channels {
        part_filtered := <-channel
        filtered_data = append(filtered_data, part_filtered...)
    }

    if *json_flag {
        print_json(&filtered_data, lang, *limit)
    } else {
        print_cli(&filtered_data, lang, *limit)
    }
}

func timeTrack(start time.Time, name string) {
    elapsed := time.Since(start)
    log.Printf("%s took %s", name, elapsed)
}

func chunks(array *[]string, number_of_chunks int) [][]string {
    var output [][]string
    data_size := int64(len(*array))
    chunk_size := int64(math.Round(float64(data_size) / float64(number_of_chunks)))
    for i := int64(0); i < data_size; i += chunk_size {
        upper_bound := i + chunk_size
        if upper_bound > data_size {
            upper_bound = data_size
        }
        part := (*array)[i:upper_bound]
        output = append(output, part)
    }
    return output
}

func stringInSlice(a *string, list []string) bool {
    for _, b := range list {
        if b == *a {
            return true
        }
    }
    return false
}

func read_language_file(path *string) []string {
    defer timeTrack(time.Now(), "read_language_file")
    language, err := ioutil.ReadFile(*path)
    if err != nil {
        log.Fatal(err)
    }
    return strings.Split(string(language), "\n")
}

func format_cli_display(language *string, title *string) string {
    var output strings.Builder
    output.WriteString(*title)
    output.WriteString(" - ")
    output.WriteString(format_url(language, title))
    return output.String()
}

func print_cli(filtered_data *[]string, language *string, limit int) {
    for index, line := range *filtered_data {
        fmt.Println(format_cli_display(language, &line))
        if index == limit {
            break
        }
    }
}

func print_json(filtered_data *[]string, language *string, limit int) {
    var json_data JsonOutput
    json_data.Size = len(*filtered_data)
    for index, line := range *filtered_data {
        json_data.Data = append(json_data.Data, Element{line, format_url(language, &line)})
        if index == limit {
            break
        }
    }
    b, err := json.Marshal(json_data)
    if err != nil {
        log.Fatal(err)
    }
    fmt.Println(string(b))
}

func format_url(language *string, title *string) string {
    var output strings.Builder
    output.WriteString("https://")
    output.WriteString(*language)
    output.WriteString(".wikipedia.org/wiki/")
    output.WriteString(*title)
    return output.String()
}

func search(data []string, input_string *string, channel chan<- []string) {
    // data is not a reference, because see https://stackoverflow.com/questions/62391832/why-is-my-go-function-parameter-having-a-different-size-before-calling-the-funct
    defer timeTrack(time.Now(), "search")
    var filtered_english []string
    for _, line := range data {
        if strings.Contains(strings.ToLower(line), *input_string) {
            filtered_english = append(filtered_english, line)
        }
    }
    channel <- filtered_english
}

type JsonOutput struct {
    Size int `json:"size"`
    Data []Element `json:"data"`
}

type Element struct {
    Title string `json:"title"`
    Url string `json:"url"`
}
