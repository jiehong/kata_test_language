FROM golang:1.14

WORKDIR /workdir

COPY data ./data
COPY wiki.go ./

RUN go run wiki.go -json 'covid'
