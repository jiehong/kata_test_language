import time
from argparse import Namespace, ArgumentParser
from concurrent.futures import Executor
from concurrent.futures.process import ProcessPoolExecutor
from concurrent.futures.thread import ThreadPoolExecutor
from dataclasses import dataclass
from typing import Callable, List, Any, Type

import jsonpickle


def main() -> None:
    args = parse_arguments()
    run(args.query, args.lang, args.json, args.limit)


def parse_arguments() -> Namespace:
    parser = ArgumentParser(description="Search Wikipedia offline")
    parser.add_argument("query", metavar="query", type=str, help="Query")
    parser.add_argument("--lang", type=str, default="en", help="Language (default: en)", choices=["en", "it", "fr"])
    parser.add_argument("--json", default=False, action="store_true", help="Json output")
    parser.add_argument("--limit", type=int, default=10, help="Limit number of results (default: 10)")

    return parser.parse_args()


def time_track(method: Callable) -> Callable:
    def timed(*args, **kw):
        start = time.time()
        try:
            return method(*args, **kw)
        finally:
            end = time.time()
            diff = (end - start) * 1000
            print(f"{method.__name__} took {diff} ms")

    return timed


@time_track
def run(query: str, language: str, is_json: bool, limit: int) -> None:
    query = query.lower()
    path = f"data/{language}"

    data = read_language_file(path)

    # Test: "covid" in English should return 378 results, no matter the
    # number of threads. This works as expected.
    number_of_threads = 4
    if number_of_threads == 1:
        filtered_data = search(data, query)
    else:
        # Warning: This ends up slower than using 1 thread, because objects need
        # to be pickled to and from other processes, making it very slow
        datas = chunks(data, number_of_threads)
        filtered_data = []
        executor_type: Type[Executor] = ProcessPoolExecutor if (number_of_threads % 2 == 0) else ThreadPoolExecutor
        with executor_type(max_workers=number_of_threads) as executor:
            futures = []
            for data in datas:
                futures.append(executor.submit(search, data, query))
            for future in futures:
                filtered_data += future.result()

    if is_json:
        print_json(filtered_data, language, limit)
    else:
        print_cli(filtered_data, language, limit)


@time_track
def read_language_file(path: str) -> List[str]:
    with open(path, 'r') as file_descriptor:
        return file_descriptor.readlines()


def search(data: List[str], query: str) -> List[str]:
    # Uses its own timer, because python does know how to
    # marshall functions correctly, and it can't be used with
    # multiprocessing, because of the @annotation
    start = time.time()
    output = []
    for line in data:
        if query in line.lower():
            output.append(line)
    end = time.time()
    diff = (end - start) * 1000
    print(f"search took {diff} ms")
    return output


@time_track
def print_cli(filtered_data: List[str], language: str, limit: int) -> None:
    for index, line in enumerate(filtered_data):
        if index > limit:
            break
        print(format_cli_display(language, line[:-1]))


def format_cli_display(language: str, title: str) -> str:
    return f"{title} - {format_url(language, title)}"


def format_url(language: str, title: str) -> str:
    return f"https://{language}.wikipedia.org/wiki/{title}"


@time_track
def print_json(filtered_data: List[str], language: str, limit: int) -> None:
    @dataclass
    class Element:
        title: str
        url: str

    @dataclass
    class JsonOutput:
        size: int
        data: List[Element]

    elements = []
    for index, title in enumerate(filtered_data):
        if index > limit:
            break
        elements.append(Element(title[:-1], format_url(language, title[:-1])))

    json_output = JsonOutput(size=len(filtered_data), data=elements)
    print(jsonpickle.dumps(json_output, unpicklable=False, indent="  "))


def chunks(array: List[Any], number_of_chunks: int) -> List[List[Any]]:
    chunk_size = round(len(array) / number_of_chunks)
    output = []
    # Iterate by "chunk_size"
    for i in range(0, len(array), chunk_size):
        # asking for a bound bigger than the size of the list
        # is automatically trimmed down to the end of the list
        part = array[i:i + chunk_size]
        output.append(part)
    return output


if __name__ == "__main__":
    main()
